#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 12 09:32:17 2018

@author: developer

link usado como base: https://medium.com/@viniljf/utilizando-processamento-de-linguagem-natural-para-criar-um-sumariza%C3%A7%C3%A3o-autom%C3%A1tica-de-textos-775cb428c84e
"""

import PyPDF2
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize

from string import punctuation
from nltk.probability import FreqDist
from collections import defaultdict
from heapq import nlargest

sent = ''

def extractTextOfUrl(url):
    link = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    pagina = urlopen(link).read().decode('utf-8', 'ignore')

    soup = BeautifulSoup(pagina, "lxml")
    #print(soup)
    texto = soup.find(id="noticia").text
    return texto


def extractTextOfPdf(pdfFile):
    #abrindo arquivo PDF
    pdfFileObject = open(pdfFile, 'rb')
    #Lendo o PDF
    pdfReader = PyPDF2.PdfFileReader(pdfFileObject)
    #Obtendo o numero de paginas do arquivo
    number_of_pages = pdfReader.numPages
    #Variável que armazena o conteúdo que será lido individualmente de cada página
    page_content = ''
    
    #A cada página lida, o conteúdo é adicionado á variável de retorno
    for page_number in range(number_of_pages):
        page = pdfReader.getPage(page_number)
        page_content = page_content + page.extractText()
        
    return page_content



def summText(texto):
    from nltk.corpus import stopwords
    summary = ''
    sentencas = sent_tokenize(texto)
    sent = sentencas
    palavras = word_tokenize(texto.lower())

    stopwords = set(stopwords.words('portuguese') + list(punctuation))
    palavras_sem_stopwords = [palavra for palavra in palavras if palavra not in stopwords]

    frequencia = FreqDist(palavras_sem_stopwords)

    sentencas_importantes = defaultdict(int)

    for i, sentenca in enumerate(sentencas):
        for palavra in word_tokenize(sentenca.lower()):
            if palavra in frequencia:
                sentencas_importantes[i] += frequencia[palavra]

    idx_sentencas_importantes = nlargest(3, sentencas_importantes, sentencas_importantes.get)

    for i in sorted(idx_sentencas_importantes):
        summary = summary + '\n'+ sentencas[i]
        
    return summary

#url = 'http://ultimosegundo.ig.com.br/mundo/2018-05-10/donald-trump-kim-namorados.html'
url = 'http://ultimosegundo.ig.com.br/politica/2017-04-25/reforma-da-previdencia.html'

textoUrl = extractTextOfUrl(url)


textoPdf = extractTextOfPdf('Sun_Tzu-A_Arte_da_Guerra.pdf')
#texto = extractTextOfPdf('Quem_Comeu_Minha_Goiabada.pdf')

print()
print('********* SUMARIZAÇÃO DA URL ************')
print()
print(summText(textoUrl))
print()
print('********* SUMARIZAÇÃO DO ARQIVO PDF ************')
print()
print(summText(textoPdf))